﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccess
{
    public static class TimeUtils
    {
        public static DateTime origin = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);

        // Convert from DateTime to Epoch time
        public static double ConvertToUnixTimestamp(DateTime date)
        {
            TimeSpan diff = date - origin;
            return diff.TotalSeconds;
        }

        // Convert from Epoch time to DateTime
        public static DateTime ConvertFromUnixTimestamp(double timestamp)
        {
            return origin.AddSeconds(timestamp).ToLocalTime();
        }
    }
}
