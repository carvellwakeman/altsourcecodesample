﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities.Models;

namespace DataAccess.DAO
{
    public class TransactionDao : ITransactionDao
    {
        private List<Transaction> mTransactions;
        private int mAutoIncrementId;

        public TransactionDao()
        {
            mTransactions = new List<Transaction>();
        }

        /// <summary>
        /// Finds all transactions associated with an account
        /// </summary>
        /// <param name="account">The Account</param>
        /// <returns>An enumerable list of transactions associated with The Account</returns>
        public IEnumerable<Transaction> GetTransactions(Account account)
        {
            return mTransactions.FindAll(t => t.AccountId == account.Id);
        }

        /// <summary>
        /// Adds a transaction to the local memory DB
        /// </summary>
        /// <param name="transaction">The transaction object to add</param>
        /// <returns>Id of newly created transaction</returns>
        public int AddTransaction(Transaction transaction)
        {
            // Set Transaction Id (normally done by DB)
            transaction.Id = ++mAutoIncrementId;
            mTransactions.Add(transaction);

            return mAutoIncrementId;
        }

    }
}
