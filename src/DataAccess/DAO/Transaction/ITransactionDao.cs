﻿using Entities.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccess
{
    public interface ITransactionDao
    {
        IEnumerable<Transaction> GetTransactions(Account accout);

        int AddTransaction(Transaction transaction);
    }
}
