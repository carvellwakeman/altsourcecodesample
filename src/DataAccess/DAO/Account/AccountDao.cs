﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities.Enum;
using Entities.Models;

namespace DataAccess.DAO
{
    public class AccountDao : IAccountDao
    {
        private List<Account> mAccounts;
        private int mAutoIncrementId;

        public AccountDao()
        {
            mAccounts = new List<Account>();
        }

        /// <summary>
        /// Finds all accounts associated with a user
        /// </summary>
        /// <param name="user">The User</param>
        /// <returns>An enumerable list of accounts associated with The User</returns>
        public IEnumerable<Account> GetAccounts(UserDisplay user)
        {
            return mAccounts.FindAll(a => a.UserId == user.Id);
        }

        /// <summary>
        /// Finds a specific account associated with a user
        /// </summary>
        /// <param name="user">The User</param>
        /// <param name="accountId">The Account to find</param>
        /// <returns>An Account instance or null</returns>
        public Account GetAccount(UserDisplay user, int accountId)
        {
            List<Account> accounts = GetAccounts(user).ToList();
            return accounts.Find(a => a.Id == accountId);
        }


        /// <summary>
        /// Adds an account to the local memory DB
        /// </summary>
        /// <param name="account">The account object to add</param>
        /// <returns>Id of newly created account</returns>
        public int AddAccount(Account account)
        {
            // Set Account Id (normally done by DB)
            account.Id = ++mAutoIncrementId;
            mAccounts.Add(account);

            return mAutoIncrementId;
        }

        /// <summary>
        /// Updates the balance on an account when a transaction is added
        /// </summary>
        /// <param name="accountId">Id of the account to update</param>
        /// <param name="transaction">Transaction to use during the update</param>
        public void UpdateBalance(UserDisplay user, int accountId, Transaction transaction)
        {
            Account account = GetAccount(user, accountId);

            if (transaction.TransactionType == TransactionType.Withdrawl.Value)
            {
                account.Balance -= transaction.Amount;
            }
            else if (transaction.TransactionType == TransactionType.Deposit.Value)
            {
                account.Balance += transaction.Amount;
            }
        }
    }
}
