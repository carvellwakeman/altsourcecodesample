﻿using Entities.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccess
{
    public interface IAccountDao
    {
        IEnumerable<Account> GetAccounts(UserDisplay user);

        Account GetAccount(UserDisplay user, int accountId);

        void UpdateBalance(UserDisplay user, int accountId, Transaction transaction);

        int AddAccount(Account account);
    }
}
