﻿using Entities.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccess
{
    public interface IUserDao
    {
        UserDisplay GetUser(string username);

        User GetUserWithPassword(string username);

        int AddUser(User user);
    }
}
