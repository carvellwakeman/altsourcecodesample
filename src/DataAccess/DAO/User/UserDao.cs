﻿using System;
using System.Collections.Generic;
using System.Text;
using Entities.Models;

namespace DataAccess.DAO
{
    public class UserDao : IUserDao
    {
        private List<User> mUsers;
        private int mAutoIncrementId;

        public UserDao()
        {
            mUsers = new List<User>();
        }

        /// <summary>
        /// Finds the first occurrence of a user by username
        /// </summary>
        /// <param name="username">The username of the user to retrieve</param>
        /// <returns>The retrieved user without a password</returns>
        public UserDisplay GetUser(string username)
        {
            User user = mUsers.Find(u => u.Username.ToLower().Equals(username.ToLower()));
            if (user != null)
            {
                return new UserDisplay() { Id = user.Id, Username = user.Username, Email = user.Email };
            }
            return null;
        }

        /// <summary>
        /// Finds the first occurrence of a user by username
        /// </summary>
        /// <param name="username">The username of the user to retrieve</param>
        /// <returns>The retrieved (complete) user</returns>
        public User GetUserWithPassword(string username)
        {
            return mUsers.Find(u => u.Username.ToLower().Equals(username.ToLower()));
        }


        /// <summary>
        /// Adds a user to the local memory DB
        /// </summary>
        /// <param name="user">The user object to add</param>
        /// <returns>Id of newly created user, -1 if user already exists</returns>
        public int AddUser(User user)
        {
            // DB Insert if not exists strategy
            User foundUser = mUsers.Find(u => u.Username.ToLower().Equals(user.Username.ToLower()));

            if (foundUser != null)
            {
                return -1;
            }

            // Set User Id (normally done by DB)
            user.Id = ++mAutoIncrementId;
            mUsers.Add(user);

            return mAutoIncrementId;
        }

    }
}
