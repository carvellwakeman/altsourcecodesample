﻿using DataAccess.Repository;
using Entities.Models;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace WebApplication.Repository
{
    public class TransactionRepository : ITransactionRepository
    {
        private string mBaseUrl;
        private HttpClient mClient;

        public TransactionRepository(IConfiguration configuration, HttpClient client)
        {
            this.mClient = client;
            this.mBaseUrl = configuration["API:URL"];
        }

        /// <summary>
        /// Makes API GET request to retrieve a list of transactions made on an account
        /// </summary>
        /// <param name="token">JWT identifying the user who made this request</param>
        /// <param name="accountId">Identifier for the user account to retrieve transactions from</param>
        /// <returns>Async ContentResult with results of web task</returns>
        public async Task<ContentResult> GetTransactions(string token, int accountId)
        {
            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get,
                mBaseUrl + "api/transactions/" + accountId.ToString());
            request.Headers.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);

            HttpResponseMessage response = await mClient.SendAsync(request);
            string accountsContent = await response.Content.ReadAsStringAsync();

            return new ContentResult() { Data = accountsContent, StatusCode = response.StatusCode };
        }

        /// <summary>
        /// Makes API POST request to add a new transaction to a particular account
        /// </summary>
        /// <param name="token">JWT identifying the user who made this request</param>
        /// <param name="accountId">Identifier for the user account to retrieve transactions from</param>
        /// <param name="transaction">Transaction object to add</param>
        /// <returns>Async ContentResult with results of web task</returns>
        public async Task<ContentResult> PostTransaction(string token, int accountId, Transaction transaction)
        {
            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post,
                mBaseUrl + "api/transactions/" + accountId.ToString());
            request.Headers.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);
            request.Content = new StringContent(JsonConvert.SerializeObject(transaction), Encoding.UTF8, "application/json");

            HttpResponseMessage response = await mClient.SendAsync(request);
            string userContent = await response.Content.ReadAsStringAsync();

            return new ContentResult() { Data = userContent, StatusCode = response.StatusCode };
        }
    }
}