﻿using Entities.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Repository
{
    public interface ITransactionRepository
    {
        Task<ContentResult> GetTransactions(string token, int accountId);

        Task<ContentResult> PostTransaction(string token, int accountId, Transaction transaction);
    }
}
