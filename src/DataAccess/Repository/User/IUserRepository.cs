﻿using Entities.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Repository
{
    public interface IUserRepository
    {
        Task<ContentResult> Login(UserLogin login);

        Task<ContentResult> AddUser(User user);
    }
}
