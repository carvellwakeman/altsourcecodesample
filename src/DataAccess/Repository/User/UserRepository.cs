﻿using DataAccess.Repository;
using Entities.Models;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace WebApplication.Repository
{
    public class UserRepository : IUserRepository
    {
        private string mBaseUrl;
        private HttpClient mClient;

        public UserRepository(IConfiguration configuration, HttpClient client) {
            this.mClient = client;
            this.mBaseUrl = configuration["API:URL"];
        }

        /// <summary>
        /// Makes API POST request to add a new user object
        /// </summary>
        /// <param name="user">User object to add</param>
        /// <returns>Async ContentResult with results of web task</returns>
        public async Task<ContentResult> AddUser(User user)
        {
            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, mBaseUrl + "api/users/");
            request.Content = new StringContent(JsonConvert.SerializeObject(user), Encoding.UTF8, "application/json");

            HttpResponseMessage response = await mClient.SendAsync(request);
            string userContent = await response.Content.ReadAsStringAsync();

            return new ContentResult() { Data = userContent, StatusCode = response.StatusCode };
        }

        /// <summary>
        /// Makes API POST request to log in as a specific user
        /// </summary>
        /// <param name="login">Truncated User object with only username and password</param>
        /// <returns>Async ContentResult with results of web task</returns>
        public async Task<ContentResult> Login(UserLogin login)
        {
            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, mBaseUrl + "api/users/login");
            request.Content = new StringContent(JsonConvert.SerializeObject(login), Encoding.UTF8, "application/json");

            HttpResponseMessage response = await mClient.SendAsync(request);
            string tokenContent = await response.Content.ReadAsStringAsync();

            return new ContentResult() { Data = tokenContent, StatusCode = response.StatusCode };
        }
    }
}