﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace DataAccess.Repository
{
    /// <summary>
    /// Describes the result of a web request
    /// </summary>
    public class ContentResult
    {
        public string Data { get; set; }
        public HttpStatusCode StatusCode { get; set; }
    }
}
