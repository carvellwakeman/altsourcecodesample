﻿using Entities.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Repository
{
    public interface IAccountRepository
    {
        Task<ContentResult> GetAccounts(string token);

        Task<ContentResult> GetAccount(string token, int accountId);
    }
}
