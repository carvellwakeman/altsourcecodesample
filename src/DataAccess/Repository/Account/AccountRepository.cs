﻿using DataAccess.Repository;
using Entities.Models;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace WebApplication.Repository
{
    public class AccountRepository : IAccountRepository
    {
        private string mBaseUrl;
        private HttpClient mClient;

        public AccountRepository(IConfiguration configuration, HttpClient client)
        {
            this.mClient = client;
            this.mBaseUrl = configuration["API:URL"];
        }

        /// <summary>
        /// Makes API GET request to retrieve a list of accounts held by a user
        /// </summary>
        /// <param name="token">JWT identifying the user who made this request</param>
        /// <returns></returns>
        public async Task<ContentResult> GetAccounts(string token)
        {
            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, mBaseUrl + "api/accounts/");
            request.Headers.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);

            HttpResponseMessage response = await mClient.SendAsync(request);
            string accountsContent = await response.Content.ReadAsStringAsync();

            return new ContentResult() { Data = accountsContent, StatusCode = response.StatusCode };
        }

        /// <summary>
        /// Makes API GET request to retrieve an account held by a user
        /// </summary>
        /// <param name="token">JWT identifying the user who made this request</param>
        /// <param name="accountId">Identifier for the user account to retrieve transactions from</param>
        /// <returns></returns>
        public async Task<ContentResult> GetAccount(string token, int accountId)
        {
            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, mBaseUrl + "api/accounts/" + accountId.ToString());
            request.Headers.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);

            HttpResponseMessage response = await mClient.SendAsync(request);
            string accountContent = await response.Content.ReadAsStringAsync();

            return new ContentResult() { Data = accountContent, StatusCode = response.StatusCode };
        }

    }
}