﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using API.Auth;
using DataAccess;
using DataAccess.DAO;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

namespace API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Dependency Injection
            services.AddSingleton<IConfiguration>(Configuration);
            services.AddSingleton<IUserDao>(new UserDao());
            services.AddSingleton<IAccountDao>(new AccountDao());
            services.AddSingleton<ITransactionDao>(new TransactionDao());
            services.AddSingleton(typeof(IJwtTokenizer), typeof(JwtTokenizer));

            // JWT
            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = "JwtBearer";
                options.DefaultChallengeScheme = "JwtBearer";
            })
            .AddJwtBearer("JwtBearer", options =>
            {
                options.TokenValidationParameters = new Microsoft.IdentityModel.Tokens.TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["API:JWT:SecretKey"])),

                    ValidateIssuer = true,
                    ValidIssuer = Configuration["API:JWT:Issuer"],

                    ValidateAudience = true,
                    ValidAudience = Configuration["API:JWT:Audience"],

                    ValidateLifetime = true,
                    ClockSkew = TimeSpan.FromMinutes(1) // Tolerance window for server time vs client time
                };
            });

            // Framework Services
            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseAuthentication();

            app.UseMvc();
        }
    }
}
