﻿using Entities.Models;
using System.Security.Claims;

namespace API.Auth
{
    public interface IJwtTokenizer
    {
        string BuildToken(UserDisplay user);

        string GetUserFromPrincipal(ClaimsPrincipal principal);
    }
}
