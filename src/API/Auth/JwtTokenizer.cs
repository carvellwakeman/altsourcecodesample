﻿using Entities.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;

namespace API.Auth
{
    public class JwtTokenizer : IJwtTokenizer
    {
        private IConfiguration mConfiguration;
        public JwtTokenizer(IConfiguration configuration)
        {
            this.mConfiguration = configuration;
        }

        /// <summary>
        /// Builds a string JWT from a user and configuration files
        /// </summary>
        /// <param name="user">The User</param>
        /// <returns>A base64 encoded JSON web token as a string</returns>
        public string BuildToken(UserDisplay user)
        {
            string secretKey = mConfiguration["API:JWT:SecretKey"];
            string issuer = mConfiguration["API:JWT:Issuer"];
            string audience = mConfiguration["API:JWT:Audience"];
            int expireMinutes = int.Parse(mConfiguration["API:JWT:ExpireMinutes"]);

            DateTime expireTime = DateTime.Now.AddMinutes(expireMinutes);

            List<Claim> claims = new List<Claim>{
                new Claim("id", user.Id.ToString()),
                new Claim("name", user.Username),
                new Claim("email", user.Email),
                new Claim("expiration", expireTime.ToString())
            };

            SymmetricSecurityKey securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(secretKey));
            SigningCredentials credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            JwtSecurityToken token = new JwtSecurityToken(
                issuer: issuer,
                audience: audience,
                claims: claims,
                expires: expireTime,
                signingCredentials: credentials
            );

            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        /// <summary>
        /// Gets a username from a ClaimsPrincipal. Principal provided by .NET Core automagically when using [Authorize] Data Annotation
        /// </summary>
        /// <param name="principal">The Claims Principal from a request</param>
        /// <returns>Username or null</returns>
        public string GetUserFromPrincipal(ClaimsPrincipal principal)
        {
            // JWT User
            if (principal.HasClaim(c => c.Type == "name"))
            {
                // Get Name from JWT claims
                Claim jwtName = principal.Claims.First(c => c.Type == "name");

                return jwtName.Value;
            }
            return null;
        }
    }
}
