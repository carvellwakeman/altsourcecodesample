﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using API.Auth;
using DataAccess;
using Entities.Enum;
using Entities.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace API.Controllers
{
    [Route("api/[controller]")]
    public class UsersController : Controller
    {
        private IJwtTokenizer mJwtTokenizer;
        private IUserDao mUserDao;
        private IAccountDao mAccountDao;

        public UsersController(IJwtTokenizer jwtTokenizer, IUserDao userDao, IAccountDao accountDao) : base()
        {
            this.mJwtTokenizer = jwtTokenizer;
            this.mUserDao = userDao;
            this.mAccountDao = accountDao;
        }

        // GET /
        [HttpGet]
        [Authorize]
        public IActionResult Get()
        {
            // User lookup from principal
            UserDisplay foundUser = mUserDao.GetUser(mJwtTokenizer.GetUserFromPrincipal(HttpContext.User));

            if (foundUser != null)
            {
                return Ok(foundUser);
            }

            return NotFound("User is not valid."); // Purposefully generic
        }

        // POST /
        [HttpPost]
        [AllowAnonymous]
        public IActionResult Post([FromBody] User user)
        {
            // Insert user
            if (mUserDao.GetUser(user.Username) == null)
            {
                // Hash+Salt user password
                string salt = BCrypt.Net.BCrypt.GenerateSalt();
                user.Password = BCrypt.Net.BCrypt.HashPassword(user.Password, salt);

                mUserDao.AddUser(user);

                // Create two default bank accounts for user
                mAccountDao.AddAccount(new Account() { AccountType=AccountType.Checkings.Value, UserId=user.Id });
                mAccountDao.AddAccount(new Account() { AccountType=AccountType.Savings.Value, UserId=user.Id });

                return Ok();
            }

            // User already exists or input could not be validated
            return BadRequest(string.Format("User already exists."));
        }

        // POST /login
        [HttpPost("login")]
        [AllowAnonymous]
        public IActionResult Login([FromBody] UserLogin user)
        {
            if (!string.IsNullOrWhiteSpace(user.Username) && !string.IsNullOrWhiteSpace(user.Password))
            {
                // Find user to login
                User foundUser = mUserDao.GetUserWithPassword(user.Username);

                if (foundUser == null)
                {
                    return StatusCode(StatusCodes.Status403Forbidden, "Wrong username or password.");
                }

                // Check provided password against found user hashed password
                bool hashedPasswordMatches = BCrypt.Net.BCrypt.Verify(user.Password, foundUser.Password);

                if (!hashedPasswordMatches)
                {
                    return StatusCode(StatusCodes.Status403Forbidden, "Wrong username or password.");
                }

                // Build JWT and return
                string token = mJwtTokenizer.BuildToken(new Entities.Models.UserDisplay() { Id = foundUser.Id, Username = foundUser.Username, Email = foundUser.Email });

                return Ok(token);
            }

            return BadRequest("No credentials provided.");
        }

    }
}
