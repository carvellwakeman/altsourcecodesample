﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using API.Auth;
using DataAccess;
using Entities.Enum;
using Entities.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace API.Controllers
{
    [Route("api/[controller]")]
    public class TransactionsController : Controller
    {
        private IJwtTokenizer mJwtTokenizer;
        private IUserDao mUserDao;
        private IAccountDao mAccountDao;
        private ITransactionDao mTransactionDao;

        public TransactionsController(IJwtTokenizer jwtTokenizer, IUserDao userDao, IAccountDao accountDao, ITransactionDao transactionDao) : base()
        {
            this.mJwtTokenizer = jwtTokenizer;
            this.mUserDao = userDao;
            this.mAccountDao = accountDao;
            this.mTransactionDao = transactionDao;
        }

        // GET /{accountId}
        [HttpGet("{accountId}")]
        [Authorize]
        public IActionResult Get(int accountId)
        {
            // User lookup from principal
            string username = mJwtTokenizer.GetUserFromPrincipal(HttpContext.User);
            if (!string.IsNullOrWhiteSpace(username))
            {
                UserDisplay foundUser = mUserDao.GetUser(username);

                if (foundUser != null)
                {
                    Account account = mAccountDao.GetAccount(foundUser, accountId);

                    if (account != null)
                    {
                        IEnumerable<Transaction> transactions = mTransactionDao.GetTransactions(account);
                        return Ok(transactions);
                    }

                    return NotFound(string.Format("Account {0} not found for User", accountId));
                }

                return NotFound("Token is expired or invalid.");
            }

            // User does not exist
            return BadRequest(string.Format("User not found."));
        }

        // POST /{accountId}
        // NOTE: I realize these two endpoints repeat most of their code - if this were NodeJS I would set up a middleware function to handle this.
        [HttpPost("{accountId}")]
        [Authorize]
        public IActionResult Post(int accountId, [FromBody]Transaction transaction)
        {
            // User lookup from principal
            string username = mJwtTokenizer.GetUserFromPrincipal(HttpContext.User);
            if (!string.IsNullOrWhiteSpace(username))
            {
                UserDisplay foundUser = mUserDao.GetUser(username);

                if (foundUser != null)
                {
                    Account account = mAccountDao.GetAccount(foundUser, accountId);

                    if (account != null)
                    {
                        // Add transaction to account
                        transaction.AccountId = accountId;
                        transaction.TimeStamp = TimeUtils.ConvertToUnixTimestamp(DateTime.UtcNow);
                        mTransactionDao.AddTransaction(transaction);
                        mAccountDao.UpdateBalance(foundUser, accountId, transaction);
                        return Ok();
                    }

                    return NotFound(string.Format("Account {0} not found for User", transaction.AccountId));
                }

                return NotFound("Token is expired or invalid.");
            }

            // User does not exist
            return BadRequest(string.Format("User not found."));
        }
    }
}
