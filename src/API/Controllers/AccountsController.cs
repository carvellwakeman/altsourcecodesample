﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using API.Auth;
using DataAccess;
using Entities.Enum;
using Entities.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace API.Controllers
{
    [Route("api/[controller]")]
    public class AccountsController : Controller
    {
        private IJwtTokenizer mJwtTokenizer;
        private IUserDao mUserDao;
        private IAccountDao mAccountDao;

        public AccountsController(IJwtTokenizer jwtTokenizer, IUserDao userDao, IAccountDao accountDao) : base()
        {
            this.mJwtTokenizer = jwtTokenizer;
            this.mUserDao = userDao;
            this.mAccountDao = accountDao;
        }

        // GET /
        [HttpGet]
        [Authorize]
        public IActionResult Get()
        {
            // User lookup from principal
            string username = mJwtTokenizer.GetUserFromPrincipal(HttpContext.User);
            if (!string.IsNullOrWhiteSpace(username))
            {
                UserDisplay foundUser = mUserDao.GetUser(username);

                if (foundUser != null)
                {
                    IEnumerable<Account> accounts = mAccountDao.GetAccounts(foundUser);
                    return Ok(accounts);
                }

                return NotFound("Token is expired or invalid.");
            }

            // User does not exist
            return BadRequest(string.Format("User not found."));
        }

        [HttpGet("{accountId}")]
        [Authorize]
        public IActionResult Get(int accountId)
        {
            // User lookup from principal
            string username = mJwtTokenizer.GetUserFromPrincipal(HttpContext.User);
            if (!string.IsNullOrWhiteSpace(username))
            {
                UserDisplay foundUser = mUserDao.GetUser(username);

                if (foundUser != null)
                {
                    Account account = mAccountDao.GetAccount(foundUser, accountId);

                    if (account != null)
                    {
                        return Ok(account);
                    }

                    return NotFound(string.Format("Account {0} not found", accountId));
                }

                return NotFound("Token is expired or invalid.");
            }

            // User does not exist
            return BadRequest(string.Format("User not found."));
        }

    }
}
