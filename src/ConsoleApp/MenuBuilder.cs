﻿using ConsoleApp.utils;
using DataAccess;
using DataAccess.Repository;
using Entities.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp
{
    /// <summary>
    /// Class used to build the structure of the menus in the application
    /// Contains structure, navigation stack, and all behavior
    /// </summary>
    public class MenuBuilder
    {
        private IAPIHelper apiHelper;
        private ApiToken apiToken;

        public MenuBuilder(IAPIHelper apiHelper)
        {
            this.apiHelper = apiHelper;
        }

        public Menu Build()
        {
            #region Create new account
            Menu registerMenu = new Menu()
            {
                Title = "Register for a new account",
                PageHeader = "Register",
                Actions = new List<MenuAction>()
                {
                    new DataEntryAction() { Prompt = "Enter Username:" },
                    new DataEntryAction() { Prompt = "Enter Email:" },
                    new DataEntryAction() { Prompt = "Enter Password:" },
                    new DataEntryAction() { Prompt = "Verify Password:" },
                    new BackgroundAction()
                    {
                        Action = delegate(List<string> actionStack)
                        {
                            // Input data
                            string username = actionStack[0];
                            string email = actionStack[1];
                            string password = actionStack[2];
                            string verifyPassword = actionStack[3];

                            // Validation
                            if (string.IsNullOrWhiteSpace(username) || string.IsNullOrWhiteSpace(email) || string.IsNullOrWhiteSpace(password))
                            {
                                Console.WriteLine("Parameter missing or invalid.");
                                Console.Write("Press Enter to Go Back:");  Console.ReadLine();
                                return;
                            }

                            if (!password.Equals(verifyPassword))
                            {
                                Console.WriteLine("Passwords do not match!");
                                Console.Write("Press Enter to Go Back:");  Console.ReadLine();
                                return;
                            }

                            // Post to API
                            ContentResult result = apiHelper.PostUser(new User(){
                                Username = username,
                                Email = email,
                                Password = password
                            });

                            // Notify of failure
                            if (result.StatusCode != System.Net.HttpStatusCode.OK)
                            {
                                Console.WriteLine(result.Data);
                                Console.Write("Press Enter to Go Back:");  Console.ReadLine();
                            }
                        }
                    }
                }
            };
            #endregion

            #region Login to account
            Menu loginMenu = new Menu()
            {
                Title = "Login",
                PageHeader = "Login",
                Actions = new List<MenuAction>()
                {
                    new DataEntryAction() { Prompt = "Enter Username:" },
                    new DataEntryAction() { Prompt = "Enter Password:" },
                    new BackgroundAction()
                    {
                        WebAction = delegate(List<string> actionStack)
                        {
                            // Input data
                            string username = actionStack[0];
                            string password = actionStack[1];

                            // Validation
                            if (string.IsNullOrWhiteSpace(username) || string.IsNullOrWhiteSpace(password))
                            {
                                Console.WriteLine("Parameter missing or invalid.");
                                Console.Write("Press Enter to Go Back:"); Console.ReadLine();
                                return null;
                            }

                            // Post to API
                            ContentResult result = apiHelper.LoginUser(new UserLogin(){
                                Username = username,
                                Password = password
                            });

                            // Store token on good response
                            if (result.StatusCode == System.Net.HttpStatusCode.OK)
                            {
                                apiToken = new ApiToken() { Token = result.Data };
                            }
                            else
                            {
                                Console.WriteLine("Invalid Username or Password.");
                                Console.Write("Press Enter to Go Back:"); Console.ReadLine();
                            }

                            return result;
                        }
                    }
                }
            };
            #endregion

            #region Quit menu
            Menu quitMenu = new Menu()
            {
                PageHeader = "Quit",
                Actions = new List<MenuAction>()
                {
                    new BackgroundAction()
                    {
                        Action = delegate(List<string> actionStack)
                        {
                            Environment.Exit(0);
                        }
                    }
                }
            };
            #endregion

            #region Logout menu
            Menu logoutMenu = new Menu()
            {
                PageHeader = "Logout",
                // Clear token
                BeforeMenuBuild = delegate (Menu menu)
                {
                    apiToken = null;
                }
            };
            #endregion

            #region Entry point menu
            Menu mainMenu = new Menu()
            {
                Title = "Banking Ledger for AltSource, by Zach Lerew",
                PageHeader = "Main Menu",
                Prompt = "Select a Task > ",
                Menus = new List<Menu>()
                {
                    registerMenu,
                    loginMenu,
                    quitMenu
                }
            };
            #endregion

            #region Open Accounts Menu
            Menu accountsMenu = new Menu()
            {
                Title = "Your Open Accounts",
                PageHeader = "Your Accounts",
                Prompt = "> ",
                BeforeMenuBuild = delegate (Menu menu)
                {
                    menu.Notes.Clear();
                    menu.Menus.Clear();

                    // Check for API token
                    if (apiToken != null && !string.IsNullOrWhiteSpace(apiToken.Token))
                    {
                        menu.Title = "Your Open Accounts";

                        menu.Menus.Add(logoutMenu);
                        List<Account> accounts = apiHelper.GetAccounts(apiToken.Token);
                        menu.Menus.AddRange(BuildAccountMenus(accounts, menu, mainMenu));
                    }
                }
            };
            #endregion

            // Build menu stack
            registerMenu.Next = mainMenu;
            loginMenu.Next = accountsMenu;
            logoutMenu.Next = mainMenu;
            accountsMenu.Next = mainMenu;

            return mainMenu;
        }

        /// <summary>
        /// Builds a list of menu objects, each corresponding to an Account model
        /// </summary>
        /// <param name="accounts">The list of accounts</param>
        /// <param name="accountsMenu">A reference to the accounts menu</param>
        /// <param name="rootMenu">A reference to the main menu</param>
        /// <returns>List of menu objects, each for an account model</returns>
        public List<Menu> BuildAccountMenus(List<Account> accounts, Menu accountsMenu, Menu rootMenu)
        {
            List<Menu> menus = new List<Menu>();

            foreach (Account a in accounts)
            {
                menus.Add(BuildMenuForAccount(a, accountsMenu, rootMenu));
            }

            return menus;
        }

        /// <summary>
        /// Builds a menu for a particular account.
        /// Includes actions for viewing history, adding transactions, and observing account balance
        /// </summary>
        /// <param name="account">The account to build a menu for</param>
        /// <param name="accountsMenu">A reference to the accounts menu</param>
        /// <param name="rootMenu">A reference to the main menu</param>
        /// <returns></returns>
        public Menu BuildMenuForAccount(Account account, Menu accountsMenu, Menu rootMenu)
        {
            // Back menu
            Menu backMenu = new Menu()
            {
                PageHeader = "Back"
            };

            #region Session expired menu
            Menu sessionExpiredMenu = new Menu()
            {
                Title = "Session Expired (Please log in again).",
                Prompt = "> ",
                Next = rootMenu,
                Menus = new List<Menu>()
                {
                    new Menu()
                    {
                        PageHeader = "Back",
                        Next = rootMenu
                    }
                }
            };
            #endregion

            #region History Menu
            Menu accountHistoryMenu = new Menu()
            {
                Title = account.AccountType + " Account History",
                PageHeader = "Account History",
                Prompt = "> ",
                Next = accountsMenu,
                Menus = new List<Menu>()
                {
                    backMenu
                },
                BeforeMenuBuild = delegate (Menu menu)
                {
                    menu.Notes.Clear();

                    // Check for API token
                    if (apiToken == null || string.IsNullOrWhiteSpace(apiToken.Token))
                    {
                        menu.Navigate = sessionExpiredMenu;
                    }
                    else
                    {
                        // Get Transactions from API 
                        List<Transaction> transactions = apiHelper.GetTransactions(apiToken.Token, account.Id);

                        if (transactions == null || transactions.Count == 0)
                        {
                            menu.Notes.Add("No Transaction Data Found");
                        }
                        else
                        {
                            // Store transaction data as notes
                            foreach (Transaction t in transactions)
                            {
                                string transactionString = TimeUtils.ConvertFromUnixTimestamp(t.TimeStamp).ToString() +
                                    "    " + t.Source +
                                    "    " + t.TransactionType +
                                    "   $" + t.Amount;

                                menu.Notes.Add(transactionString);
                            }
                            menu.Notes.Add("-- Most Recent Displayed at the Bottom --");
                        }
                    }
                }
            };
            #endregion

            #region Transaction Menu
            Menu transactionMenu = new Menu()
            {
                Title = "Withdrawl or Deposit",
                PageHeader = "Make a Withdrawl or Deposit",
                Prompt = "> ",
                Next = accountsMenu,
                Actions = new List<MenuAction>()
                {
                    new DataEntryAction() { Prompt = "Enter Type (0=Withdrawl, 1=Deposit):" },
                    new DataEntryAction() { Prompt = "Enter Source:" },
                    new DataEntryAction() { Prompt = "Enter Amount:" },
                    new BackgroundAction()
                    {
                        Action = delegate(List<string> actionStack)
                        {
                            // Input data
                            int transactionType;
                            double amount;
                            string source = actionStack[1];
                            bool aConversionResult = Double.TryParse(actionStack[2], out amount);
                            bool ttConversionResult = Int32.TryParse(actionStack[0], out transactionType);

                            // Validation
                            if (string.IsNullOrWhiteSpace(source) || !ttConversionResult || !aConversionResult)
                            {
                                Console.WriteLine("Parameter missing or invalid, Please try again:");
                                Console.Write("Press Enter to Go Back:"); Console.ReadLine();
                                return;
                            }

                            // Check for API token
                            if (apiToken != null && !string.IsNullOrWhiteSpace(apiToken.Token))
                            {
                                // Post to API
                                ContentResult result = apiHelper.PostTransaction(apiToken.Token, account.Id,
                                    new Transaction(){
                                        Source = source,
                                        Amount = amount,
                                        TransactionType = new string[]{ "Withdrawl", "Deposit" }[transactionType]
                                    }
                                );

                                // Notify of failure
                                if (result.StatusCode != System.Net.HttpStatusCode.OK)
                                {
                                    Console.WriteLine(result.Data);
                                    Console.Write("Something went Wrong, Press Enter to Go Back:");  Console.ReadLine();
                                }
                            }
                        }
                    }
                }
            };
            #endregion

            #region Account Details Menu
            Menu accountDetailsMenu = new Menu()
            {
                Title = account.AccountType + " Account",
                PageHeader = account.AccountType + " (Balance: $" + account.Balance.ToString() + ")",
                Prompt = "> ",
                Menus = new List<Menu>()
                {
                    new Menu()
                    {
                        PageHeader = "Back",
                        Next = accountsMenu
                    },
                    accountHistoryMenu,
                    transactionMenu
                },
                BeforeMenuBuild = delegate (Menu menu)
                {
                    // Check for API token
                    if (apiToken == null || string.IsNullOrWhiteSpace(apiToken.Token))
                    {
                        menu.Navigate = sessionExpiredMenu;
                    }
                    else
                    {
                        Account newAccount = apiHelper.GetAccount(apiToken.Token, account.Id);

                        // Update account balance
                        if (newAccount != null)
                        {
                            menu.Title = account.AccountType + " (Balance: $" + account.Balance.ToString() + ")";
                        } else
                        {
                            menu.Navigate = sessionExpiredMenu;
                        }
                    }
                }
            };
            #endregion

            // Setup menu stack
            accountDetailsMenu.Next = accountDetailsMenu;
            accountHistoryMenu.Next = accountDetailsMenu;
            transactionMenu.Next = accountDetailsMenu;
            backMenu.Next = accountDetailsMenu;

            return accountDetailsMenu;
        }

    }
}
