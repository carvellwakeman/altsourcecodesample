﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp
{
    /// <summary>
    /// Action describing the retrieval of data from the user.
    /// E.g. "Enter Username:"
    /// </summary>
    public class DataEntryAction : MenuAction
    {
        public override string Execute(List<string> actionStack)
        {
            string input;
            do
            {
                Console.Write(Prompt);
                input = Console.ReadLine();
            } while (string.IsNullOrWhiteSpace(input));
            return input;
        }
    }
}
