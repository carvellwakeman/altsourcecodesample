﻿using DataAccess.Repository;
using Entities.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp.utils
{
    public interface IAPIHelper
    {
        // User
        ContentResult PostUser(User user);
        ContentResult LoginUser(UserLogin user);

        // Accounts
        List<Account> GetAccounts(string token);
        Account GetAccount(string token, int accountId);


        // Transactions
        List<Transaction> GetTransactions(string token, int accountId);
        ContentResult PostTransaction(string token, int accountId, Transaction transaction);

    }
}
