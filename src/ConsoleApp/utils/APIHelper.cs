﻿using DataAccess.Repository;
using Entities.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp.utils
{
    /// <summary>
    /// This is a simple wrapper class that makes calls to instances of several repositories.
    /// </summary>
    public class APIHelper : IAPIHelper
    {
        public int Timeout { get; set; }

        private IUserRepository mUserRepository;
        private IAccountRepository mAccountRepository;
        private ITransactionRepository mTransactionRepository;

        public APIHelper(IUserRepository userRepository, IAccountRepository accountRepository, ITransactionRepository transactionRepository)
        {
            this.mUserRepository = userRepository;
            this.mAccountRepository = accountRepository;
            this.mTransactionRepository = transactionRepository;

            this.Timeout = 5000; // 5 Second default timeout
        }

        // User
        public ContentResult PostUser(User user)
        {
            Task<ContentResult> task = mUserRepository.AddUser(user);
            task.Wait(Timeout);

            return task.Result;
        }

        public ContentResult LoginUser(UserLogin user)
        {
            Task<ContentResult> task = mUserRepository.Login(user);
            task.Wait(Timeout);

            return task.Result;
        }

        // Accounts
        public List<Account> GetAccounts(string token)
        {
            Task<ContentResult> task = mAccountRepository.GetAccounts(token);
            task.Wait(5000);

            return JsonConvert.DeserializeObject<List<Account>>(task.Result.Data);
        }

        public Account GetAccount(string token, int accountId)
        {
            Task<ContentResult> task = mAccountRepository.GetAccount(token, accountId);
            task.Wait(5000);

            return JsonConvert.DeserializeObject<Account>(task.Result.Data);
        }

        // Transactions
        public List<Transaction> GetTransactions(string token, int accountId)
        {
            Task<ContentResult> task = mTransactionRepository.GetTransactions(token, accountId);
            task.Wait(Timeout);

            return JsonConvert.DeserializeObject<List<Transaction>>(task.Result.Data);
        }

        public ContentResult PostTransaction(string token, int accountId, Transaction transaction)
        {
            Task<ContentResult> task = mTransactionRepository.PostTransaction(token, accountId, transaction);
            task.Wait(Timeout);

            return task.Result;
        }
    }
}
