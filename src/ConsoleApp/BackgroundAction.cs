﻿using DataAccess.Repository;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp
{
    /// <summary>
    /// Background action taken by a menu, usually after some amount of user input
    /// </summary>
    public class BackgroundAction : MenuAction
    {
        public override string Execute(List<string> actionStack)
        {
            // Execute action delegate
            if (Action != null)
            {
                Action.Invoke(actionStack);
            }

            // Execute Web Action delegate
            if (WebAction != null)
            {
                return WebAction.Invoke(actionStack).Data;
            }

            return null;
        }
    }
}
