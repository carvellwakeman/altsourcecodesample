﻿using DataAccess.Repository;
using System;
using System.Collections.Generic;
using System.Net.Http;
using WebApplication.Repository;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using System.IO;
using ConsoleApp.utils;
using Entities.Models;
using System.Linq;
using System.Threading.Tasks;

namespace ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            // Configuration file
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);

            IConfigurationRoot Configuration = builder.Build();

            // configure services
            var services = new ServiceCollection();
            services.AddSingleton<IConfiguration>(Configuration);
            services.AddSingleton(new HttpClient());
            services.AddSingleton(typeof(MenuBuilder));
            services.AddSingleton(typeof(IAPIHelper), typeof(APIHelper));
            services.AddSingleton(typeof(IUserRepository), typeof(UserRepository));
            services.AddSingleton(typeof(IAccountRepository), typeof(AccountRepository));
            services.AddSingleton(typeof(ITransactionRepository), typeof(TransactionRepository));

            // create a service provider from the service collection
            var serviceProvider = services.BuildServiceProvider();

            // Dependencies
            MenuBuilder menuBuilder = (MenuBuilder)serviceProvider.GetService(typeof(MenuBuilder));

            // Build menus
            Menu mainMenu = menuBuilder.Build();

            Menu activeMenu = mainMenu;
            while (activeMenu != null)
            {
                activeMenu = activeMenu.Execute();
                Console.Clear();
            }
        }

    }
}
