﻿using DataAccess.Repository;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp
{
    /// <summary>
    /// An action that can be taken within a menu.
    /// Action can be a client action or a web request
    /// </summary>
    public abstract class MenuAction
    {
        public delegate ContentResult WebTask(List<string> actionStack);
        public delegate void Task(List<string> actionStack);

        public string Prompt { get; set; }
        public Task Action { get; set; }
        public WebTask WebAction { get; set; }

        public abstract string Execute(List<string> actionStack);
    }
}
