﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp
{
    /// <summary>
    /// Describes a menu and all of its options.
    /// Contains header seen when selecting the menu,
    /// Title seen when inside the menu,
    /// Prompt seen when selecting an item from the menu,
    /// Notes seen when inside the menu (these describe the status of the menu),
    /// Next 
    /// </summary>
    public class Menu
    {
        public delegate void MenuBuiltTask(Menu menu);

        // Seen when inside the menu
        public string Title { get; set; }
        // Used when menu is a navigation target of another menu
        public string PageHeader { get; set; }
        // Printed when menu prompts for a selection
        public string Prompt { get; set; }
        // List of sub menus
        public List<Menu> Menus { get; set; }
        // Used to describe state for this part of the application
        public List<string> Notes { get; set; }
        // Menu to navigate to next if no user selection is made
        public Menu Next { get; set; }
        // Navigation override taken for some actions
        public Menu Navigate { get; set; }
        // Actions that can be taken inside this menu
        public List<MenuAction> Actions { get; set; }

        // Result of last action taken in menu
        public List<string> LastActionStack { get; set; }
        // Called before presenting the menu to the user
        public MenuBuiltTask BeforeMenuBuild { get; set; }

        public Menu()
        {
            Menus = new List<Menu>();
            Actions = new List<MenuAction>();
            Notes = new List<string>();
            LastActionStack = new List<string>();
        }

        // Display the menu and its options
        public void Write()
        {
            Console.WriteLine("\n"+Title);

            // Write notes
            if (Notes != null)
            {
                foreach (string n in Notes)
                {
                    Console.WriteLine(n);
                }
            }

            // Write menus
            if (Menus != null)
            {
                int i = 1;
                foreach (Menu m in Menus)
                {
                    Console.WriteLine("{0}) {1}", i++, m.PageHeader);
                }
            }
        }

        // Prompt user for choice and perform the chosen action or navigation
        public Menu Execute()
        {
            // Execute delegate before writing
            if (BeforeMenuBuild != null)
            {
                BeforeMenuBuild.Invoke(this);
            }

            // Navigate if told to
            if (Navigate != null)
            {
                Menu navTo = Navigate;
                Navigate = null;
                return navTo;
            }

            // Write menu to console
            this.Write();

            // Execute actions
            if (Actions != null && Actions.Count > 0)
            {
                // Build action stack from action calls
                List<string> actionResults = new List<string>();
                foreach (MenuAction a in Actions)
                {
                    string contentResult = a.Execute(actionResults);
                    if (contentResult != null)
                    {
                        actionResults.Add(contentResult);
                    }
                }

                // Store action stack
                LastActionStack = actionResults;

                // Go to next menu if one exists
                if (Next != null)
                {
                    return Next;
                } else
                {
                    // Navigate to this menu again
                    return this;
                }
            } else
            {
                // Select menu option
                if (Menus != null && Menus.Count > 0)
                {
                    int option = 0;
                    do
                    {
                        Console.Write(Prompt);
                        Int32.TryParse(Console.ReadLine(), out option);

                    } while ((option-1) < 0 || (option-1) >= Menus.Count);

                    // Navigate to menu
                    return Menus[option-1];
                }

                return Next;
            }

        }

    }
}
