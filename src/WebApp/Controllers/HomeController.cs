using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using DataAccess.Repository;
using Entities.Models;
using Entities.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace WebApp.Controllers
{
    /// <summary>
    /// This is a simple wrapper class that makes calls to instances of several repositories.
    /// </summary>
    public class HomeController : Controller
    {
        private IUserRepository mUserRepository;
        private IAccountRepository mAccountRepository;
        private ITransactionRepository mTransactionRepository;

        public HomeController(IUserRepository userRepository, IAccountRepository accountRepository, ITransactionRepository transactionRepository)
        {
            this.mUserRepository = userRepository;
            this.mAccountRepository = accountRepository;
            this.mTransactionRepository = transactionRepository;
        }

        public IActionResult Index()
        {
            return View();
        }


        // User
        [Route("postUser")]
        public async Task<DataAccess.Repository.ContentResult> PostUser([FromBody] User user)
        {
            return await mUserRepository.AddUser(user);
        }

        [Route("loginUser")]
        public async Task<DataAccess.Repository.ContentResult> LoginUser([FromBody] UserLogin login)
        {
            return await mUserRepository.Login(login);
        }

        // Accounts
        [Route("getAccounts")]
        public async Task<DataAccess.Repository.ContentResult> GetAccounts([FromBody] ApiToken token)
        {
            return await mAccountRepository.GetAccounts(token.Token);
        }

        [Route("getAccount")]
        public async Task<DataAccess.Repository.ContentResult> GetAccount([FromBody] GetAccountViewModel viewModel)
        {
            return await mAccountRepository.GetAccount(viewModel.token, viewModel.accountId);
        }

        // Transactions
        [Route("getTransactions")]
        public async Task<DataAccess.Repository.ContentResult> GetTransactions([FromBody] GetAccountViewModel viewModel)
        {
            return await mTransactionRepository.GetTransactions(viewModel.token, viewModel.accountId);
        }

        [Route("postTransaction")]
        public async Task<DataAccess.Repository.ContentResult> PostTransaction([FromBody] PostTransactionViewModel viewModel)
        {
            return await mTransactionRepository.PostTransaction(viewModel.token, viewModel.accountId, viewModel.transaction);
        }


        public IActionResult Error()
        {
            ViewData["RequestId"] = Activity.Current?.Id ?? HttpContext.TraceIdentifier;
            return View();
        }
    }
}
