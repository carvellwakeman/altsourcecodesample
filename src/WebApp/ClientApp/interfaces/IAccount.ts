﻿// Describes an account model used in rendering
export interface IAccount {
    id: number;
    accountType: string;
    balance: number;
}