﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Entities.Models
{
    /// <summary>
    /// Common parent to all models, contains an Id. Normally not needed as a database would provide Ids
    /// </summary>
    public abstract class BaseEntity
    {
        public int Id { get; set; }
    }
}
