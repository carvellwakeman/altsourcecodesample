﻿using Entities.Enum;
using System;
using System.Collections.Generic;
using System.Text;

namespace Entities.Models
{
    /// <summary>
    /// A user account. Contains transactions and a balance.
    /// </summary>
    public class Account : BaseEntity
    {
        public string AccountType { get; set; }
        public double Balance { get; set; }
        public int UserId { get; set; }
    }
}
