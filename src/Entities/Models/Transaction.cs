﻿using Entities.Enum;
using System;
using System.Collections.Generic;
using System.Text;

namespace Entities.Models
{
    /// <summary>
    /// A transaction between a Source and the user for an Amount of currency.
    /// </summary>
    public class Transaction : BaseEntity
    {
        public string TransactionType { get; set; }
        public double Amount { get; set; }
        public string Source { get; set; }
        public double TimeStamp { get; set; }
        public int AccountId { get; set; }
    }
}
