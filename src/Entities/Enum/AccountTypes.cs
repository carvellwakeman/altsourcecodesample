﻿
namespace Entities.Enum
{
    /// <summary>
    /// Describes the kinds of accounts that can be held by a user
    /// </summary>
    public class AccountType
    {
        public string Value { get; set; }

        public override string ToString()
        {
            return Value;
        }

        public static AccountType Checkings { get { return new AccountType() { Value = "Checkings" }; } }
        public static AccountType Savings { get { return new AccountType() { Value = "Savings" }; } }
    }
}
