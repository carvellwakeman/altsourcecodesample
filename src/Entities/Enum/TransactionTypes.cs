﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entities.Enum
{
    /// <summary>
    /// Describes the types of transactions that can be made on an account by a user
    /// </summary>
    public class TransactionType
    {
        public string Value { get; set; }

        public override string ToString()
        {
            return Value;
        }

        public static TransactionType Deposit { get { return new TransactionType() { Value = "Deposit" }; } }
        public static TransactionType Withdrawl { get { return new TransactionType() { Value = "Withdrawl" }; } }
    }
}
