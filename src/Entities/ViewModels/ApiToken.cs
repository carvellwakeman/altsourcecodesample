﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entities.Models
{
    /// <summary>
    /// Used for simpler model binding on requests, stores a string JWT
    /// </summary>
    public class ApiToken
    {
        public string Token { get; set; }
    }
}
