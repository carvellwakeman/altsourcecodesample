﻿using Entities.Models;

namespace Entities.ViewModels
{
    /// <summary>
    /// View model describing the addition of a transaction 
    /// </summary>
    public class PostTransactionViewModel
    {
        public string token { get; set; }
        public int accountId { get; set; }
        public Transaction transaction { get; set; }
    }
}
