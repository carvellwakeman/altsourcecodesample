﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Entities.Models
{
    /// <summary>
    /// View model provided by a client to authenticate a user
    /// </summary>
    public class UserLogin
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
