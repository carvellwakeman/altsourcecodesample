﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Entities.Models
{
    /// <summary>
    /// View model describing a user's information without a password
    /// </summary>
    public class UserDisplay : BaseEntity
    {
        public string Username { get; set; }
        public string Email { get; set; }
    }
}
