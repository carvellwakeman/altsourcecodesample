﻿using Entities.Models;

namespace Entities.ViewModels
{
    /// <summary>
    /// View model used for retrieving or adding to an account
    /// </summary>
    public class GetAccountViewModel
    {
        public string token { get; set; }
        public int accountId { get; set; }
    }
}
